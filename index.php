<?php get_header(); ?>

<div class="row">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php $feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="card m-2" style="width: 14rem auto;">
                    <?php if ($feat_image) : ?>
                        <img class="card-img-top" src="<?php echo $feat_image ?>" alt="Card image cap">
                    <? endif; ?>
                    <div class="card-body">
                        <h5 class="card-title"><?php the_title(); ?></h5>
                        <p class="card-text"><?php the_excerpt(); ?></p>
                        <a href="<?php the_permalink(); ?>" class="btn btn-primary">Ver más</a>
                    </div>
                </div>
            </div>
        <?php endwhile;
    else : ?>
        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
    <?php endif; ?>

</div>

<?php get_footer(); ?>